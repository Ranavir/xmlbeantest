package com.stl.xmltest;

import java.io.File;
import java.io.IOException;

import org.apache.xmlbeans.XmlException;

import com.tcube.evaluat.schemas.queries.EValuaTDocument;
import com.tcube.evaluat.schemas.queries.EValuaTType;
import com.tcube.evaluat.schemas.queries.EValuaTdaosType;
import com.tcube.evaluat.schemas.queries.MemberDetailsDAOType;
import com.tcube.evaluat.schemas.queries.QueryType;

public class FindQuery {
	 static EValuaTDocument  valuaTDocument=null;
	 static EValuaTType eValuaTType=null;
	 static EValuaTdaosType eValuaTdaosType=null;
	 
	public static void main(String[] args) {
		getXmlDoc();
		String query = getQueries(MemberConstant.MEMBER_DETAILS_DAO, MemberConstant.GETLOYALTYID);
		System.out.println(query);
	}
	public static String getQueries( int type, String queryName) {
		 //OutputLogger.logIt("Checking file"+" at getQueries "+queryName, Levels.INFO, logPath);
		String query=null;		
		QueryType[] queryType = getQueryType(type);
		for(int i=0;i<queryType.length;i++){				
			if(queryType[i].getName().equals(queryName)){
				query=queryType[i].getStringValue();				
			}
		}
		//OutputLogger.logIt("query"+query, Levels.INFO, logPath);
		return query;
	}
	public static QueryType[] getQueryType(int type){
		//OutputLogger.logIt("getQueryType"+type, Levels.INFO, logPath);
		QueryType[] queryType = null;
		switch(type){
		case 1:
			MemberDetailsDAOType memberDetailsDAOType=eValuaTdaosType.getMemberDetailsDAO();
			queryType =memberDetailsDAOType.getQueryArray();			
			break;
	/*	case 2:
			ContactDetailDAOType contactDetailDAOType=eValuaTdaosType.getContactDetailDAO();
			queryType =contactDetailDAOType.getQueryArray();
			break;
		*/
		default :
			
			break;
		}
		return queryType;
	}

	public static void getXmlDoc(){
		File evaluat = null;
		
		 try{
			 evaluat = new File("schemas/eValuaT.xml");
			 System.out.println("Path of XML file is : "+evaluat.getAbsolutePath());
			//OutputLogger.logIt("Checking file"+evaluat.exists(), Levels.INFO, logPath);
			//OutputLogger.logIt("Checking file path"+evaluat.getAbsolutePath(), Levels.INFO, logPath);
			 valuaTDocument=EValuaTDocument.Factory.parse(evaluat);
			 
			 eValuaTType=valuaTDocument.getEValuaT();
			 
			// OutputLogger.logIt("Checking file class :"+eValuaTType.getClass(), Levels.INFO, logPath);
			 eValuaTdaosType=eValuaTType.getEValuaTdaos();
			
		 	}catch (XmlException exm)
		    {
		 		exm.printStackTrace();
			// OutputLogger.logIt("Checking file XMLEX "+exm.getMessage(), Levels.ERROR, logPath);
		    } catch (IOException e)
		    {
		    	e.printStackTrace();
		    //	OutputLogger.logIt("Checking file IOEX "+e.getMessage(), Levels.ERROR, logPath);
		    }
		    catch (Exception ex)
		    {
		    	ex.printStackTrace();
		    	//OutputLogger.logIt("Checking file EX "+ex.getMessage(), Levels.ERROR, logPath);
		    }	
	}
}
