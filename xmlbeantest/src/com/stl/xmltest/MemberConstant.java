/*
 *  Copyright (c) 2011 TCube Solutions, All Rights Reserved.
 *
 *  This code is confidential to TCube Solutions and
 *  shall not be disclosed outside the TCube without the
 *  prior written permission of the Director of Technology
 *  Delivery.
 *
 *  In the event that such disclosure is permitted the
 *  code shall not be copied or distributed other than
 *  on a need-to-know basis and any recipients may be
 *  required to sign a confidentiality undertaking in
 *  favour of TCube.
 */
package com.stl.xmltest;

/**
 * @author Nibedita
 * Date : Jan 11, 2012
 */
public class MemberConstant {
	/*
	 * contains value to select  dao present in xml schemas 
	 */
	
	public static final  int MEMBER_DETAILS_DAO=1;
	/*
	 * contains value to call the sql queries for Membership management
	 */
	public static final String GETLOYALTYID="GetLoyatyId";
	public static final String MEMEBERLOGIN = "MemberLogin";
	public static final String GETMEMBERDETAILS="GetMemberDetails";
	public static final String CONTACTINSERT="ContactInsert";
	public static final String INSERTMEMBERCONTACTDETAILS="InsertMemberdetails";
	public static final String GETMYDETILS="GetMyDetails";
	public static final String GETBRANCHNAME="GetBranchName";
	public static final String UPDATELOGINFLAG="UpdateLogCount";
	public static final String GETLATESTLOYALITYID="GetLatestLoyalityId";
	public static final String GETMEMBERSERVICEDETAILS = "GetMemberServiceDetails";
	public static final String GET_CARDTYPE_FOR_MEMBER = "GET_CARDTYPE_FOR_MEMBER";
	public static final String GET_CARD_DETAILS = "GET_CARD_DETAILS";
	public static final String GETMEMBERCONTACTSEARCH = "GetMemberContactSearch";
	public static final String UPDATEMEMBERDETAILS="UpdateMemberDetails";
	public static final String GETFORGOTPASSWORD="GetForgotPassword";
	public static final String MEMBERCHANGEPASWORD="MemberChangePassword";
	public static final String GETOLDPASSWORD="GetOldPassword";
	

	public static final String GETALLUSERID="GetAllUserId";
	public static final String GETLOYALTYUSERID="GetLoyaltyUserId";

	public static final String GETACCOUNTDETAILS="GET_ACCOUNT_DETAILS";
	
	public static final String MEMBERCHANGEUSERID="MemberChangeUserId";
	
	public static final String GET_ALL_COMMUNICATION_HISTORY = "GET_ALL_COMMUNICATION_HISTORY";
	public static final String INSERT_COMMUNICATION_MAIL_DETAIL="INSERT_COMMUNICATION_MAIL_DETAIL";
	public static final String GET_ALL_FEEDBACK_HISTORY="GET_ALL_FEEDBACK_HISTORY";
	public static final String GET_ALL_COMMUNICATION_HISTORY_DYNAMIC="GET_ALL_COMMUNICATION_HISTORY_DYNAMIC";
	public static final String GET_ALL_FEEDBACK_HISTORY_DYNAMIC="GET_ALL_FEEDBACK_HISTORY_DYNAMIC";
	public static final String GET_ALL_CATEGORIES = "GET_ALL_CATEGORIES";
	public static final String GET_ALL_REASONS = "GET_ALL_REASONS";
	public static final String GET_ALL_MAIL_DETAILS = "GET_ALL_MAIL_DETAILS";
	public static final String GET_ALL_FEEDBACK_DETAILS = "GET_ALL_FEEDBACK_DETAILS";
	public static final String CHECK_MEMBER_OFFER_DETAILS_EXIST = "CHECK_MEMBER_OFFER_DETAILS_EXIST";
	public static final String INSERT_MEMBER_VOUCHER_DETAILS = "INSERT_MEMBER_VOUCHER_DETAILS";
}
